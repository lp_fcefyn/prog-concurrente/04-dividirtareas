# 04-DividirTareas

Si cada trabaja sobre una porción de los datos sin que ningún otro hilo trabaje sobre la misma no habrá un recurso compartido que proteger y por lo tanto tampoco indeterminismo.