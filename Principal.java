

public class Principal {
	
	public static void main (String[] args)
	{
		Runtime r = Runtime.getRuntime();
		int n = r.availableProcessors();
		Hilo h[] = new Hilo[n];
		int start = 0;
		int rango = Hilo.tam/n;
		int finish = rango;
		
		for(int i = 0; i < Hilo.vec.length; i++)
		{
			Hilo.vec[i]= i+1;
		}
		
		for(int i = 0; i < n; i++)
		{
			if(i != n-1)
			{
				h[i] = new Hilo(start,finish);
				h[i].start();
				
				start = finish;
				finish += rango;
			}
			else
			{
				h[i] = new Hilo(start,Hilo.tam);
				h[i].start();
			}
		}																							
		
		try {												
			for(int i = 0; i < n; i++)		
			{										
				h[i].join();			
			}																				
		}catch(Exception ex) {}							
		
		System.out.println("Vector: ");
		for(int i = 0; i < Hilo.vec.length; i++)
		{
			System.out.println("["+Hilo.vec[i]+"] ");
		}
	}
}
