// Multiplicar todos los elementos de un vector por 10. Notese que si el vector es muy pequeño no se justifica usar hilos.
public class Hilo extends Thread{
	public static int tam = 15;
	public static int vec[] = new int[tam];
	private int inicio;
	private int fin;
	
	public Hilo(int inicio, int fin)
	{
		this.inicio = inicio;
		this.fin = fin;
	}
	
	public void run()
	{
		for(int i=inicio;i < fin; i++)
		{
			vec[i] = vec[i] * 10;
		}
	}
	
}
